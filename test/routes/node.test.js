
process.env.NODE_ENV = 'test';

var chai = require('chai');
var chaiHTTP = require('chai-http');
var app = require('../../src/server.js');
var should = chai.should();

chai.use(chaiHTTP);

describe('Route: /api/node', () => {
  describe('(GET) /', () => {
    it('Should return json', (done) => {
      chai.request(app.server).get('/api/node').end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        done();
      }); 
    });
  });
  //describe('(POST) /', () => {});
});

app.db.close();

