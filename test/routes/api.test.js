
process.env.NODE_ENV = 'test';

let mongoose = require('mongoose');
var chai = require('chai');
var chaiHTTP = require('chai-http');
var app = require('../../src/server.js');
var should = chai.should();

chai.use(chaiHTTP);

describe('Route: /api', () => {
  describe('(GET) /', () => {
    it('Should return the API name and version number', (done) => {
      chai.request(app.server).get('/api').end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('API').eql('Keg Monitoring');
        res.body.should.have.property('Version').eql('v1.0.0');
        done();
      }); 
    });
  });
});

app.db.close();
