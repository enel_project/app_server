
process.env.NODE_ENV = 'test';

let mongoose = require('mongoose');
let chai = require('chai');
let should = chai.should();

let Keg = require('../../src/controllers/models/keg.model.js');

describe('Model: keg', () => {
  it('Should return error if required fields are empty', (done) => {
    var keg = new Keg();
    keg.validate((err) => {
      (err.errors.name == undefined).should.equal(false);
      (err.errors.family == undefined).should.equal(false);
      done();
    });
  });
  it('Should not return error if required fields are included', (done) => {
    var keg = new Keg({
      name : 'test', 
      family : {
        flow : 1,
        voltage : 2,
        temperature : 3
      },
    });
    keg.validate((err) => { 
      (err == undefined).should.equal(true);
      done();
    });
  });
});
