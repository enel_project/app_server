
process.env.NODE_ENV = 'test';

let mongoose = require('mongoose');
let chai = require('chai');
let should = chai.should();

let Sensor = require('../../src/controllers/models/sensor.model.js');

describe('Model: sensor', () => {
  it('Should return error if required fields are empty', (done) => {
    var sensor = new Sensor();
    sensor.validate((err) => {
      (err.errors.level == undefined).should.equal(false);
      (err.errors.voltage == undefined).should.equal(false);
      (err.errors.temperature == undefined).should.equal(false); 
      done();
    });
  });
  it('Should not return error if required fields are included', (done) => {
    var sensor = new Sensor({level : 1, voltage : 2, temperature : 3});
    sensor.validate((err) => { 
      (err == undefined).should.equal(true);
      done();
    });
  });
});
