
process.env.NODE_ENV = 'test';

let mongoose = require('mongoose');
let chai = require('chai');
let should = chai.should();

let User = require('../../src/controllers/models/user.model.js');

describe('Model: user', () => {
  it('Should return error if required fields are empty', (done) => {
    var user = new User();
    user.validate((err) => {
      (err.errors.name == undefined).should.equal(false);
      (err.errors.password == undefined).should.equal(false);
      done();
    });
  });
  it('Should not return error if required fields are included', (done) => {
    var user = new User({name : 'test', password : 'testpass'});
    user.validate((err) => { 
      (err == undefined).should.equal(true);
      done();
    });
  });
});

