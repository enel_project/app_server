
let app = require('./server.js');
let config = require('config');

app.server.listen(config.port, () => {
  console.log("Listening on " + config.host + ":" + config.port);
});

