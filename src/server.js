
let express = require('express');
let router = require('./router.js');
let app = module.exports = express();
let config = require('config');
let morgan = require('morgan');
let bodyParser = require('body-parser');
let validator = require('express-validator');
let mongoose = require('mongoose');
let cors = require('cors');

// Create DB connection
let dbOptions = {useNewUrlParser: true};
mongoose.connect(config.DBHost, dbOptions);
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));


console.log('NODE_ENV: ' + config.util.getEnv('NODE_ENV'));

// Enable logger if not in test mode
if (config.util.getEnv('NODE_ENV') != 'test') {
  app.use(morgan('combined'));
}

// Enable cross origin requests
app.use(cors({origin : '*'}));

// Parse json, post data, and raw text and move to body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.text());
app.use(bodyParser.json({type: 'application/json'}));
app.use(validator());

// Use router endpoints
app.use('/', router);

module.exports = { server: app, db: db};

