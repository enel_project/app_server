
let config = require('config');
let ObjectId = require('mongoose').Types.ObjectId;
let User = require('../models/user.model.js');
let Network = require('../models/network.model.js');
let Keg = require('../models/keg.model.js');
let Sensor = require('../models/sensor.model.js');

module.exports = {
  get : (req, res) => {
    var uid = res.locals.decoded.id;
    if (!uid) {
      res.status(400).json({'error' : config.error.request.bad});
    } else {
      User.find({
        _id : uid
      }, '-_id -__V -password').lean().exec((err, user) => {
        if (err) throw err;
        if (!user || !user.length) {
          res.status(200).json({'error' : config.error.user.missing});
        } else {
          res.status(200).json(user); 
        }
      });
    }
  },
  network : {
    get : (req, res) => {
      var uid = res.locals.decoded.id;
      var nid = req.params.network_id;
      if (!uid || !nid) {
        res.status(400).json({'error' : config.error.request.bad});
      } else {
        Network.find({
          user : uid,
          network: nid
        }).select("-_id -__V").lean().exec((err, network) => {
          if (err) throw err;
          if (!network || !network.length) {
            res.status(200).json({'error' : config.error.network.missing});
          } else {
            res.status(200).json(network);
          }
        });
      }
    }
  },
  keg : {
    get : (req, res) => {
      var uid = res.locals.decoded.id;
      var nid = req.params.network_id;
      var keg = req.params.keg_id; 
      if (!uid || !nid || !keg) {
        res.status(400).json({'error' : config.error.request.bad});
      } else {
        Keg.find({
          network : nid,
          keg : keg
        }).select("-_id -__V").lean().exec((err, keg) => {
          if (err) throw err;
          if (!keg || !keg.length) {
            res.status(200).json({'error' : config.error.keg.missing});
          } else {
            res.status(200).json({'keg' : keg});
          }
        });
      }
    }, 
    patch : (req, res) => {
      var uid = res.locals.decoded.id;
      var nid = req.params.network_id;
      var keg = req.params.keg_id;
      var key = req.body.key;
      var value = req.body.value;
      var supported = ['name', 'size', 'reset'];
      if (!uid || !nid || !keg || !key || !value) {
        res.status(400).json({'error' : config.error.request.bad});
      } else {
        if (supported.includes(key) && (key in Keg.schema.tree)) {
          Keg.findOne({
            network : nid,
            keg : keg
          }, (err, keg) => {
            if (err) throw err;
            if (!keg) {
              res.status(200).json({'error' : config.error.keg.missing});
            } else { 
              keg[key] = value;
              keg.save((err) => {
                res.status(200).json({'keg' : keg});
              });
            }
          });
        } else {
          res.status(400).json({'error' : config.error.request.supported});
        }
      }
    }
  },
  graph : {
    range_convert : (req, res, next) => {
      var date = req.query.date;
      var range = req.query.range;
      if (!date || !range) {
        res.status(400).json({'error' : config.error.request.bad});
      } else {
        var search_range = range.match(/[a-zA-Z]/).pop();
        var conversion = 0;
        range = Number(range.slice(0, range.indexOf(search_range)));
        switch (search_range) {
          case ('m'): 
            conversion = 60;
            break;
          case ('h'):
            conversion = 3600;
            break;
          case ('d'):
            conversion = 86400;
            break;
          case ('m'):
            conversion = 2678400;
            break;
          case ('y'):
            conversion = 32140800;
            break;
          default:
            res.status(200).json({'error' : config.error.graph.time});
            return;
           break; 
        }
        date = res.locals.date.getTime()/1000 - range * conversion;
        res.locals.range = new Date(0);
        res.locals.range.setUTCSeconds(date);
        next();
      } 
    },
    utc_convert: (req, res, next) => {
      var date = req.query.date;
      if (!date) {
        res.status(400).json({'error' : config.error.request.bad});
      } else {
        res.locals.date = new Date(0);        
        res.locals.date.setUTCSeconds(date / 1000);
        next();
      }
    },
    user : {
      get : (req, res) => {
        var uid = res.locals.decoded.id;
        if (!uid) {
          res.status(400).json({'error' : config.error.request.bad});
        } else {
          User.findOne({
            _id : uid
          }).lean().select("name networks admin").exec((err, user) => {
            if (err) throw err;
            if (!user) {
              res.status(200).json({'error' : config.error.user.missing});
            } else {
              Network.find({
                network : { $in : user.networks }
              }).lean().select("-_id name network kegs").exec((err, network) => {
                if (err) throw err;
                if (!network || !network.length) {
                  res.status(200).json(user);
                } else {
                  user.networks = [...network];
                  var keg_id = [];
                  for (i = 0; i < user.networks.length; i++) {
                    keg_id = keg_id.concat(user.networks[i].kegs);
                  }
                  Keg.find({
                    keg : { $in : keg_id }
                  }).lean().select("-_id name network keg size reset").exec((err, keg) => {
                    if (err) throw err;
                    if (!keg || !keg.length) {
                      res.status(200).json(user);
                    } else {
                      kegs = [...keg];
                      for (i = 0; i < user.networks.length; i++) {
                        user.networks[i].kegs = [...kegs.filter(x => x.network == user.networks[i].network)];
                      }
                      var oldest_reset = res.locals.range;
                      for (i = 0; i < user.networks.length; i++) {
                        for (j = 0; j < user.networks[i].kegs.length; j++) {
                          if (oldest_reset > user.networks[i].kegs[j].reset) {
                            oldest_reset = user.networks[i].kegs[j].reset;
                          }
                        }
                      }
                      Sensor.find({
                        keg : { $in : keg_id },
                        timestamp : { $gt : oldest_reset },
                        level : { $ne : 0 }
                      }).lean().select("-_id keg timestamp level pressure temperature voltage").exec((err, sensor) => {
                        if (err) throw err;
                        if (!sensor || !sensor.length) {
                          for (i = 0; i < user.networks.length; i++) {
                            for (j = 0; j < user.networks[i].kegs.length; j++) {
                              user.networks[i].kegs[j].consumed = 0
                              user.networks[i].kegs[j].sensors = [];
                            }
                          }
                          res.status(200).json(user);
                        } else {
                          sensors = [...sensor];
                          for (i = 0; i < user.networks.length; i++) {
                            for (j = 0; j < user.networks[i].kegs.length; j++) {
                              if (user.networks[i].kegs[j]) {
                                user.networks[i].kegs[j].sensors = [...sensors.filter(x => x.keg == user.networks[i].kegs[j].keg)];
                              }
                            }
                          }
                          var consumed = 0;
                          for (i = 0; i < user.networks.length; i++) {
                            for (j = 0; j < user.networks[i].kegs.length; j++) {
                              sensors = [];
                              for (k = 0; k < user.networks[i].kegs[j].sensors.length; k++) {
                                if (user.networks[i].kegs[j].sensors[k].timestamp >= user.networks[i].kegs[j].reset) {
                                  consumed += user.networks[i].kegs[j].sensors[k].level;
                                }
                                var timestamp = user.networks[i].kegs[j].sensors[k].timestamp;
                                if (timestamp < res.locals.range || timestamp > res.locals.date) {
                                  //user.networks[i].kegs[j].sensors.splice(k, 1);
                                } else {
                                  sensors.push(user.networks[i].kegs[j].sensors[k]);
                                }
                              }
                              user.networks[i].kegs[j].consumed = consumed;
                              consumed = 0;
                              user.networks[i].kegs[j].sensors = sensors;
                            }
                          }

                          res.status(200).json(user);
                        } 
                      });
                    }
                  });
                } 
              })
            }
          });
        }
      }
    },
    network : {
      get : (req, res) => {
        var uid = res.locals.decoded.id;
        var nid = req.params.network_id;
        if (!uid || !nid) {
          res.status(400).json({'error' : config.error.request.bad});
        } else {
          Network.findOne({
            network : nid,
            user : uid
          }).lean().select("name network kegs").exec((err, network) => {
            if (err) throw err;
            if (!network) {
              res.status(200).json({'error' : config.error.network.missing});
            } else {
              Keg.find({
                network : network.network
              }).lean().select("name network keg size reset").exec((err, keg) => {
                if (err) throw err;
                if (!keg || !keg.length) {
                  res.status(200).json(network);
                } else {
                  kegs = [...keg];
                  kegs_list = network.kegs;
                  network.kegs = kegs;
                  var oldest_reset = res.locals.range;
                  for (i = 0; i < network.kegs.length; i++) {
                    if (oldest_reset > network.kegs[i].reset) {
                      oldest_reset = network.kegs[i].reset;
                    }
                  }
                  Sensor.find({
                    keg : { $in : kegs_list },
                    timestamp : { $gt : oldest_reset },
                    level : { $ne : 0 }
                  }).lean().select("keg timestamp level pressure temperature voltage").exec((err, sensor) => {
                    if (err) throw err;
                    if (!sensor || !sensor.length) {
                      for (i = 0; i < network.kegs.length; i++) {
                        network.kegs[i].consumed = 0;
                        network.kegs[i].sensors = [];
                      }
                      res.status(200).json(network);
                    } else {
                      sensors = [...sensor];
                      for (i = 0; i < network.kegs.length; i++) {
                        network.kegs[i].sensors = [...sensors.filter(x => x.keg == network.kegs[i].keg)];
                      }
                      var consumed = 0;
                      var sensors = [];
                      for (i = 0; i < network.kegs.length; i++) {
                        sensors = [];
                        for (j = 0; j < network.kegs[i].sensors.length; j++) {
                          if (network.kegs[i].sensors[j].timestamp >= network.kegs[i].reset) {
                            consumed += network.kegs[i].sensors[j].level;
                          }
                          var timestamp = network.kegs[i].sensors[j].timestamp;
                          if (timestamp < res.locals.range || timestamp > res.locals.date) {
                            //network.kegs[i].sensors.splice(j, 1);
                          } else {
                            sensors.push(network.kegs[i].sensors[j]);
                          }
                        }
                        network.kegs[i].sensors = sensors;
                        network.kegs[i].consumed = consumed;
                        consumed = 0;
                      }
                      res.status(200).json(network);
                    } 
                  });
                }
              });
            } 
          })
        }
      }
    },
    keg : {
      get : (req, res) => {
        var uid = res.locals.decoded.id;
        var nid = req.params.network_id;
        var keg = req.params.keg_id;
        if (!uid || !nid || !keg) {
          res.status(400).json({'error' : config.error.request.bad});
        } else {
          Keg.findOne({
            keg : keg,
            network : nid
          }).select('-__v').lean().exec((err, keg) => {
            if (err) throw err;
            if (!keg) {
              res.status(200).json({'error' : config.error.keg.missing});
            } else {
              var oldest_reset = 0;
              if (res.locals.range > keg.reset) {
                oldest_reset = keg.reset;
              }
              Sensor.find({
                keg : keg.keg,
                timestamp : { $gt : oldest_reset},
                level : { $ne : 0 }
              }).select("-__V").lean().exec((err, sensor) => {
                if (err) throw err;
                if (!sensor || !sensor.length) {
                  keg.consumed = 0;
                  keg.sensors = [];
                  res.status(200).json(keg);
                } else {
                  keg.sensors = [...sensor];
                  var consumed = 0;
                  sensors = [];
                  for (i = 0; i < keg.sensors.length; i++) {
                    if (keg.sensors[i].timestamp >= keg.reset) {
                      consumed += keg.sensors[i].level;
                    }
                    var timestamp = keg.sensors[i].timestamp;
                    if (timestamp < res.locals.range || timestamp > res.locals.date) {
                      //keg.sensors.splice(i, 1);
                    } else {
                      sensors.push(keg.sensors[i]);
                    }
                  }
                  keg.sensors = sensors;
                  keg.consumed = consumed;
                  res.status(200).json(keg);
                }
              });
            }
          });
        }
      }
    }
  }
}
