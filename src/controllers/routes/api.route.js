
let config = require('config');

module.exports = {
  get: (req, res) => {
    res.status(200).json({ "API" : "Keg Monitoring", "Version" : config.version });
  }
}
