
let config = require('config');
let User = require('../models/user.model.js');
let Network = require('../models/network.model.js');
let Keg = require('../models/keg.model.js');
let Sensor = require('../models/sensor.model.js');

module.exports = {
  get : (req, res) => {
    res.send("WSN GET");
  },
  put : (req, res) => {
    data = req.body;
    Keg.findOne(
      { keg : data[0].keg }
    ).select("calibration.zero calibration.span").lean().exec((err, keg) => { 
      if (!keg) {
        res.status(400).json({"error" : config.error.keg.missing});
      } else {
        for (var i = 0; i < data.length; i++) {
          data[i].temperature = 100*(3.3*data[i].temperature/4095) - 50;
          data[i].voltage = (3.3*data[i].voltage / 4095);
          data[i].level = (data[i].level / 6036) * keg.calibration.span + keg.calibration.zero;
        }
        Sensor.create(data, err => {
          if (err) throw err;
          console.log(data)
          var response = {
            success : true
          }
          res.status(200).json(response);
        }); 
      }
    });
  }
};
