

let config = require('config');
let ObjectId = require('mongoose').Types.ObjectId;
let bcrypt = require('bcrypt');
let User = require('../models/user.model.js');
let Network = require('../models/network.model.js');
let Keg = require('../models/keg.model.js');

module.exports = {
  user : {
    get : (req, res) => {
      User.find({admin:0}).select("-password -__v").exec((err, users) => {
        if (err) throw err;
        if (!users || !users.length) { 
          res.status(200).json({'error' : config.error.user.missing});
        } else {
          res.status(200).json(users);
        }
      });
    },
    post : (req, res) => {
    },
    put : (req, res) => {
      var name = req.body.name;
      var password = req.body.password;
      var admin = req.body.admin;
      if (!name || !password) {
        res.status(400).json({'error' : config.error.request.bad});;
      } else {
        User.findOne({
          name: name
        }, (err, user) => {
          if (err) throw err;
          if (!user) {
            bcrypt.hash(password, 10, (err, hash) => {
              var user = new User({name : name, password : hash, admin : admin});
              user.save(err => {
                if (err) throw err;
                var response = {
                  type : user.admin ? 'admin' : 'user',
                  name : user.name,
                  id : user._id
                };
                res.status(200).json(response);
              });
            }); 
          } else {
            res.status(200).json({'error' : config.error.user.exists});
          }
        });  
      }
    },
    patch : (req, res) => {
    },
    delete : (req, res) => {
      var user_id = req.body.user_id;
      if (!user_id) {
        res.status(400).json({'error' : config.error.request.bad});;
      } else {
        User.findOne({
          _id : user_id
        }, (err, user) => {
          if (err) throw err;
          if (!user) {
            res.status(200).json({'error' : config.error.user.missing});
          } else {
            var response = {
              name : user.name,
            }
            user.remove();
            res.status(200).json(response);
          }
        });
      }
    }
  },
  network : {
    get : (req, res) => {
      let uid = req.params.user_id;
      let nid = req.params.network;
      Network.find({user:uid}).select("network name timestamp kegs").exec((err, networks) => {
        if (err) throw err;
        if (!networks || !networks.length) { 
          res.status(200).json({'error' : config.error.network.missing});
        } else {
          res.status(200).json(networks);
        }
      });
    },
    post : (req, res) => {
    },
    put : (req, res) => {
      var name = req.body.name;
      var nid = req.body.network;
      var uid = req.params.user_id;
      if (!name || !uid || !nid) {
        res.status(400).json({'error' : config.error.request.bad});;
      } else {
        User.find({
          _id : uid
        }, (err, user) => {
          if (err) throw err;
          if (!user || !user.length) {
            res.status(200).json({'error' : config.error.user.missing});
          } else {
            Network.find({
              network : nid
            }, (err, network) => {
              if (err) throw err;
              if (!network || !network.length) {
                var network = new Network({name : name, user : uid, network : nid});
                network.save(err => {
                  if (err) throw err;
                  res.status(200).json({'network' : network});
                });
              } else {
                res.status(200).json({'error' : config.error.network.exists});
              }
            });
          }
        });  
      }
    },
    patch : (req, res) => {
    },
    delete : (req, res) => {
      var network_id = req.body.network_id;
      if (!network_id) {
        res.status(400).json({'error' : config.error.request.bad});;
      } else {
        Network.findOne({
          _id : network_id
        }, (err, network) => {
          if (err) throw err;
          if (!network) {
            res.status(200).json({'error' : config.error.network.missing});
          } else {
            var response = {
              id : network._id,
              user_id : network.user_id,
              name : network.name,
              timestamp : network.timestamp
            }
            res.status(200).json(response);
            User.updateOne({
              _id : network.user
            }, {
              $pull : { 'networks' : network._id }
            }, null, (err, user) => {
              if (err) throw err;
              network.remove();
            });
          }
        });
      }
    }
  },
  keg : {
    get : (req, res) => {
      let uid = req.params.user_id;
      let nid = req.params.network_id;
      Keg.find({network:nid}).select("id name family calibration timestamp").exec((err, kegs) => {
        if (err) throw err;
        if (!kegs || !kegs.length) { 
          res.status(200).json({'error' : config.error.keg.missing});
        } else {
          res.status(200).json(kegs);
        }
      });
    },
    post : (req, res) => {
    },
    put : (req, res) => {
      var nid = req.params.network_id;
      var uid = req.params.user_id;
      var mac = req.body.mac;
      var name = req.body.name;
      var family_flow = req.body.family_flow;
      var family_voltage = req.body.family_voltage;
      var family_temperature = req.body.family_temperature;
      if (!name || !uid || !mac || !nid || !family_flow || !family_voltage || !family_temperature) {
        res.status(400).json({'error' : config.error.request.bad});;
      } else {
        Network.find({
          network : nid
        }, (err, network) => {
          if (!network || !network.length) {
            res.status(200).json({'error' : config.error.network.missing});
          } else {
            Keg.findOne({
              network : nid,
              name : name
            }, (err, keg) => {
              if (err) throw err;
              if (!keg) {
                var keg = new Keg({
                  name : name, 
                  keg : mac,
                  network : nid,
                  family : {
                    flow : family_flow,
                    voltage : family_voltage,
                    temperature : family_temperature
                  }
                });
                keg.save(err => {
                  if (err) throw err;
                  res.status(200).json({'keg' : keg});
                });
              } else {
                res.status(200).json({'error' : config.error.keg.exists});
              }
            });  
          }
        });
      }
    },
    patch : (req, res) => {
    },
    delete : (req, res) => {
      var keg_id = req.body.keg_id;
      if (!keg_id) {
        res.status(400).json({'error' : config.error.request.bad});;
      } else {
        Keg.findOne({
          _id : keg_id
        }, (err, keg) => {
          if (err) throw err;
          if (!keg) {
            res.status(200).json({'error' : config.error.keg.missing});
          } else {
            var response = {
              id : keg._id,
              network : keg.network,
              name : keg.name,
              family : keg.family,
              calibration : keg.calibration,
              timestamp : keg.timestamp
            }
            res.status(200).json(response);
            Network.updateOne({
              _id : keg.network
            }, {
              $pull : { 'kegs' : keg._id }
            }, null, (err, network) => {
              keg.remove();
            });
          }
        });
      }
    }
  }
}
