
let config = require('config');
let jwt = require('jsonwebtoken');
let User = require('../models/user.model.js');
let Token = require('../models/token.model.js');
let bcrypt = require('bcrypt');

module.exports = {
  post: (req, res) => {
    var name = req.body.name;
    var password = req.body.password;

    if (!name || !password) {
      res.status(400).json({'error' : config.error.request.bad});
    } else {
      User.findOne({
        name:name
      }, (err, user) => {
        if (err) throw err;
        if (!user) {
          res.status(200).json({'error' : config.error.user.missing});
        } else {
          var payload = {
            type : ((user.admin) ? 'admin' : 'user'),
            name : user.name,
            id : user._id
          };
          var options = {expiresIn : '24h'};
          var jwt_token = jwt.sign(payload, config.JWTSecret, options);
          Token.findOne({
            user : user._id
          }, (err, token) => {
            if (err) throw err;
              bcrypt.compare(password, user.password, (err, result) => {
                if (err) throw err;
                if (result && token && token.valid) {
                  res.status(200).json({'name' : user.name, 'token' : token.token});
                } else if (result && token && !token.valid) {
                  token.token = jwt_token;
                  token.valid = true;
                  token.save((err) => {
                    if (err) throw err;
                    res.status(200).json({'name' : user.name, 'token' : token.token});
                  }); 
                } else if (result && !token) {
                  var new_token = new Token({
                    user : user._id,
                    token : jwt_token,
                    valid : true  
                  });
                  new_token.save((err) => {
                    if (err) throw err;
                    res.status(200).json({'name' : user.name, 'token' : new_token.token});
                  }); 
                } else if (!result && token && !token.valid && token.token === password) {
                  token.token = jwt_token;
                  token.valid = true;
                  token.save((err) => {
                    if (err) throw err;
                    res.status(200).json({'name' : user.name, 'token' : token.token});
                  }); 
                } else {
                  res.status(401).json({'error' : config.error.user.missing});
                }
              });
          });
        }
      });
    }
  },
  delete: (req, res) => {
    Token.findOne({
      user : res.locals.decoded.id,
      token : res.locals.token
    }, (err, token) => {
      if (err) throw err;
      if (!token) {
        res.status(200).json({'error' : config.error.token.missing});
      } else {
        token.valid = false;
        token.save((err) => {
          if (err) throw err;
          res.status(200).json({'token' : token.token});
        });
      }
    });
  },
  auth: (req, res, next) => {
    let jwt_token = req.body.token || req.query.token || req.headers['x-access-token'];
    res.locals.token = jwt_token;
    if (jwt_token) {
      jwt.verify(jwt_token, config.JWTSecret, (err, decoded) => {
        if (err) {
          if (err.name == 'TokenExpiredError') {
            Token.findOne({
              token : jwt_token
            }, (err, token) => {
              if (err) throw err;
              if (!token) {
                res.status(200).json({'error' : config.error.token.missing});
              } else {
                token.valid = false;
                token.save((err) => {
                  if (err) throw err;
                  res.status(401).json({'error' : config.error.token.expired});
                });
              }
            });
          } else {
            throw err;
          }
        } else {
          Token.findOne({
            user : decoded.id,
            token : jwt_token
          }, (err, token) => {
            if (err) throw err;
            if (!token) {
              res.status(200).json({'error' : config.error.token.missing});
            } else {
              if (token.valid) {
                Token.findOne({
                  user : decoded.id,
                  token : jwt_token
                }, (err, token) => {
                  if (err) throw err;
                  if (!token) {
                    res.status(200).json({'error' : config.error.token.missing});
                  } else {
                    if (token.valid) {
                      res.locals.decoded = decoded;
                      next();
                    } else {
                      res.status(200).json({'error' : config.error.token.invalid});
                    }
                  }
                });
              }
            }
          });
        }
      });
    } else {
      res.status(401).send({'error' : config.error.token.missing});
    }
  },
  auth_user: (req, res, next) => { 
    if (!res.locals.decoded.type) {
      res.status(401).json({'error' : config.error.token.invalid});
    } else {
      if (res.locals.decoded.type == 'user' || res.locals.decoded.type == 'admin') {
        next();
      } else {
        res.status(401).send({'error' : config.error.token.invalid});
      }
    }
  },
  auth_admin: (req, res, next) => {
    if (!res.locals.decoded.type) {
      res.status(401).json({'error' : config.error.token.invalid});
    } else {
      if (res.locals.decoded.type == 'admin') {
        next();
      } else {
        res.status(401).send({'error' : config.error.token.invalid});
      }
    }
  },
  auth_wsn: (req, res, next) => {
    if (!res.locals.decoded.type) {
      res.status(401).json({'error' : config.error.token.invalid});
    } else {
      if (res.locals.decoded.type == 'wsn') {
        next();
      } else {
        res.status(401).send({'error' : config.error.token.invalid});
      }
    }
  }
}
