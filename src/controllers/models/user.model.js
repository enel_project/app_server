
let mongoose = require('mongoose');

let userSchema = new mongoose.Schema({
  name:       {type: String, required: true},
  password:   {type: String, required: true},
  admin:      {type: Boolean, default: 0},
  timestamp:  {type: Date, default: Date.now},
  networks:   [{type: Number, ref: 'network'}]
});

userSchema.pre('save', function(next) {
  now = new Date();
  if (!this.timestamp) {
    this.timestamp = now;
  }
  next();
});

userSchema.pre('remove', function(next) {
  this.model('network').remove({user : this._id}, next)
});

module.exports = mongoose.model('user', userSchema);
