
let mongoose = require('mongoose');

let networkSchema = new mongoose.Schema({
  user:           {type: mongoose.Schema.Types.ObjectId, ref: 'user'},
  network:        {type: Number, required: true},
  name:           {type: String, required: true},
  timestamp:      {type: Date, default: Date.now},
  kegs:           [{type: Number, ref: 'keg'}]
});

networkSchema.pre('save', function(next) {
  now = new Date();
  if (!this.timestamp) {
    this.timestamp = now;
  }
  this.model('user').updateOne(
      {_id : this.user},
      {$push : { networks : this.network } }
  ).exec();
  next();
});

networkSchema.pre('remove', function(next) {
  this.model('keg').remove({network : this.network}, next);
});

module.exports = mongoose.model('network', networkSchema);
