
var mongoose = require('mongoose');
var assert = require('assert');
const defines = require('../defines.js');

let _db;

function initDB() {
  if (_db) {
    console.warn("Database has already been initialized");
    return _db;
  } else {
    mongoose.connect(defines.MONGODB);
    _db = mongoose.connection; 
    assert.ok(_db, "Database connection failed");
    return _db;
  }
}

function getDB() {
  assert.ok(_db, "Database connection has not been initialized");
  return _db;
}

module.exports = {
  initDB,
  getDB
}
