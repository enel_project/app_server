
let mongoose = require('mongoose');

let kegSchema = new mongoose.Schema({
  network:      {type: Number, ref: 'network'},
  keg:          {type: Number, required: true},
  name:         {type: String, required: true},
  family:       {
                  flow:         {type: String, required: true},
                  voltage:      {type: String, required: true},
                  temperature:  {type: String, required: true}
                },
  calibration:  {
                  zero: {type: Number, default: 0},
                  span: {type: Number, default: 1}
                },
  size:         {type: Number, default: 55},
  reset:        {type: Date, default: Date.now},
  timestamp:    {type: Date, default: Date.now},
  sensors:      [{type: Number, ref: 'sensor'}]
});

kegSchema.pre('save', function(next) {
  now = new Date();
  if (!this.timestamp) {
    this.timestamp = now;
  }

  this.model('network').updateOne(
      { network : this.network },
      { $push : { kegs : this.keg} }
  ).exec();

  next();
});

kegSchema.pre('remove', function(next) {
  this.model('sensor').remove({keg : this.keg}, next);
});

module.exports = mongoose.model('keg', kegSchema);

