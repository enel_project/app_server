
let mongoose = require('mongoose');

let controlSchema = new mongoose.Schema({
  network:      {type: Number, ref: 'network'},
  target:       {type: Number, required: true},
  action:       {type: String, required: true},
  timestamp:    {type: Date, default: Date.now},
});

controlSchema.pre('save', function(next) {
  now = new Date();
  if (!this.timestamp) {
    this.timestamp = now;
  }

  next();
});

module.exports = mongoose.model('keg', kegSchema);

