
let mongoose = require('mongoose');

let tokenSchema = new mongoose.Schema({
  user:       {type: mongoose.Schema.Types.ObjectId, ref: 'user'},
  token:      {type: String, required: true},
  valid:      {type: Boolean, required: true},
  timestamp:  {type: Date, default: Date.now}
});

tokenSchema.pre('save', function(next) {
  now = new Date();
  if (!this.timestamp) {
    this.timestamp = now;
  }
  next();
});

module.exports = mongoose.model('token', tokenSchema);
