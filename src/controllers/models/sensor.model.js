
let mongoose = require('mongoose');

let sensorSchema = new mongoose.Schema({
  keg:          {type: Number, ref: 'keg'},
  level:        {type: Number, required: true},
  voltage:      {type: Number, required: true},
  temperature:  {type: Number, required: true},
  pressure:     {type: Number, required: true},
  timestamp:    {type: Date, default: Date.now}
});

module.exports = mongoose.model('sensor', sensorSchema);

