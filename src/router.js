
var express = require('express');
var router = express.Router();

let api = require('./controllers/routes/api.route.js');
let wsn = require('./controllers/routes/wsn.route.js');
let authenticate = require('./controllers/routes/authenticate.route.js');
let user = require('./controllers/routes/user.route.js');
let admin = require('./controllers/routes/admin.route.js');
let Sensor = require('./controllers/models/sensor.model.js');

// Return version number
router.route('/api')
  .get(api.get);

// Login/Logout user
router.route('/api/authenticate')
  .post(authenticate.post) // Check credentials and return token
  .delete(authenticate.auth, authenticate.delete); // Blacklist token

// (TEMPORARY) Check token contents
router.use('/api/check', authenticate.auth);

router.get('/api/check', (req, res) => {res.status(200).json({'token' : req.decoded, 'success' : 'Authenticated'});});
router.post('/api/check', (req, res) => {res.status(200).json({'token' : req.decoded, 'success' : 'Authenticated'});});
// (TEMPORARY) Add sensor reading
router.put('/api/addsensor/:keg_id', (req, res) => {
  var sensor = new Sensor({
    keg : req.params.keg_id,
    level : req.body.flow,
    voltage : req.body.voltage,
    temperature : req.body.temperature
  });
  sensor.save((err) => {
    if (err) throw err;
    res.json(sensor);
  });
});

// User Routes
router.use('/api/user', authenticate.auth, authenticate.auth_user);
router.use('/api/user/graph', user.graph.utc_convert, user.graph.range_convert);

router.route('/api/user')
  .get(user.get); //  Get user data
router.route('/api/user/graph')
  .get(user.graph.user.get); // Return graph data for user
router.route('/api/user/graph/:network_id')
  .get(user.graph.network.get); // Return graph data for network
router.route('/api/user/graph/:network_id/:keg_id')
  .get(user.graph.keg.get); // Return graph data for keg
router.route('/api/user/:network_id')
  .get(user.network.get); // Get network data
router.route('/api/user/:network_id/:keg_id')
  .get(user.keg.get)    // Get keg data
  .patch(user.keg.patch);  // Change keg information

// Admin Routes
router.use('/api/admin', authenticate.auth, authenticate.auth_admin);

router.route('/api/admin/user')
  .get(admin.user.get) // Get all users
  .post(admin.user.post) // Edit user
  .put(admin.user.put) // Add user
  .patch(admin.user.patch) // Edit user in place
  .delete(admin.user.delete); // Delete user
router.route('/api/admin/user/:user_id')
  .get(admin.network.get) // Get all networks of a user
  .post(admin.network.post) // Edit network
  .put(admin.network.put) // Add network
  .patch(admin.network.patch) // Edit network in place
  .delete(admin.network.delete); // Delete network
router.route('/api/admin/user/:user_id/:network_id')
  .get(admin.keg.get) // Get all kegs of a network
  .post(admin.keg.post) // Edit keg
  .put(admin.keg.put) // Add keg
  .patch(admin.keg.patch) // Edit keg in place
  .delete(admin.keg.delete); // Delete keg

// WSN Routes
// router.use('/api/wsn', authenticate.auth, authenticate.auth_wsn);

router.route('/api/wsn')
  .get(wsn.get)
  .put(wsn.put);


module.exports = router;

