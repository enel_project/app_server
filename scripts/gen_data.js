
var Sensor = require('../src/controllers/models/sensor.model.js');
let config = require('config');
let mongoose = require('mongoose');

var args = process.argv.slice(2);

// Create DB connection
let dbOptions = {useNewUrlParser: true};
mongoose.connect(config.DBHost, dbOptions);
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));

const READINGS_IN_DAY = 2880;
const OPENED_PROBABILITY = 0.08;
const CLOSED_PROBABILITY = 0.01;

if (args[1]) {
  var date = new Date(args[1]);
} else {
  var date = new Date();
}

date.setDate(date.getDate() - 1);

var reading = {
  keg : args[0],
  level : 0,
  voltage : 5,
  temperature : 5,
  timestamp : date.getTime()
}

reading.timestamp -= READINGS_IN_DAY;

var sensors = [];

var plot = function(reading, pos, count) { 
  var hours = new Date(reading.timestamp).getHours();
  if (hours > 2 && hours < 9) {
    var reading_prob = CLOSED_PROBABILITY;
  } else {
    var reading_prob = OPENED_PROBABILITY;
  }
  var test = Math.random();
  if (pos) {
    if (test < reading_prob) {
      count++;
      var sensor = new Sensor(reading);
      sensors.push(sensor);
      reading.level = 10 * Math.random() + 0.01;
      reading.voltage = 5 + Math.random();
      reading.temperature = 2.5 + 5 * Math.random(); 
    }
    reading.timestamp += 30000;
    plot(reading, --pos, count);
  } else {
    console.log("Done, count: " + count);
  } 
}

plot(reading, 50, 0);

Sensor.insertMany(sensors, (err) => {
  console.log('Saved sensors');
  process.exit();
});

